class CreateSeries < ActiveRecord::Migration[6.0]
  def change
    create_table :series do |t|
      t.references :worker, null: false, foreign_key: true
      t.string :name, null: true

      t.timestamps
    end
  end
end
