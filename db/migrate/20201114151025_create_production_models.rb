class CreateProductionModels < ActiveRecord::Migration[6.0]
  def change
    create_table :production_models do |t|
      t.references :series, null: false, foreign_key: true

      t.timestamps
    end
  end
end
